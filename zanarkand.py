#!/usr/bin/env python3

# Standard imports
import os
import logging
from time import sleep
from random import choices
from collections import deque
from argparse import ArgumentParser

# Third party imports
import docker
from discord_webhook import DiscordWebhook
from yaml import safe_load, YAMLError, safe_dump

def find_containers(name, label=None):
    '''
    Return the list of containers with a given name
    '''
    client = docker.from_env()
    labels = []
    if label:
        for k, v in label.items():
            labels.append(f"{k}={v}")
    return client.containers.list(all=True,
                                  filters={"name": name.replace(" ", "_"),
                                           "label": labels})

class Playlist:
    def __init__(self, name, config):
        self.name = name
        self.display_name = config[name]["display_name"]
        self.total_loops = config[name].get("loops", 1)
        self.ending = config[name].get("ending", 1)
        
class Stream:
    '''
    Overall stream object. It contains information about what's currently being played, the order of media to play,
    current loop, etc.
    '''
    def __init__(self):
        '''
        Creates the Stream object
        '''

        # Get configuration settings
        with open("/resources/config.yaml", mode="r", encoding="utf-8") as input_config:
            try:
                self.config = safe_load(input_config)
            except YAMLError as yerr:
                logging.error("Couldn't read yaml config file: %s", yerr)

        # Get current status
        with open("/resources/status.yaml", "r", encoding="utf-8") as input_status:
            try:
                status = safe_load(input_status)
            except YAMLError as yerr:
                logging.error("Couldn't read the yaml config file: %s", yerr)
        
        with open("/resources/schedule.yaml", "r", encoding="utf-8") as schedule:
            try:
                self.schedule = safe_load(schedule)
            except YAMLError as yerr:
                logging.error("Couldn't read the yaml config file: %s", yerr)

        # Set stream attributes
        self.episode = status["episode"]
        self.loop = status["loop"]
        self.position = status["position"]
        self.game = Playlist(self.schedule[self.position], self.config)
        self.container = None
        self.next_container = None
        self.length = len(self.schedule.keys()) - 1

        # Check if current episode is currently running
        client = docker.from_env()
        client.images.pull("registry.gitlab.com/docker203/ffmpeg:latest")
        containers = client.containers.list(all=True, filters={"label": ["image=ffmpeg"]})
        for container in containers:
            if container.labels["game"] == self.game.name and container.labels["episode"] == str(self.episode):
                self.container = container
            else:
                container.stop()
                container.remove()

        # If current episode is not found, create it
        if not self.container:
            self.container = self.create_ffmpeg(self.game.name, self.episode)
        
        self.set_subtitles()

    def next_video(self):
        '''
        Set the stream to play the next episode
        '''
        if self.episode == self.game.ending:
            self.episode = 1
            if self.loop == self.game.total_loops:
                self.position += 1
                if self.position > self.length:
                    self.position = 0
                self.loop = 1
                self.game = Playlist(self.schedule[self.position], self.config)
            else:
                self.loop += 1
        else:
            self.episode += 1

    def set_subtitles(self):
        '''
        Create "subtitles" that display the order of the games

        Inputs:
        ffmpeg_opts         [dict] Dictionary of ffmpeg options used
        '''
        try:
            with open('/resources/template.ass', 'r', encoding='utf-8') as st:
                content = st.read().rstrip()
            content += f" {self.game.display_name} Episode {self.episode}/{self.game.ending} Loop {self.loop}/{self.game.total_loops}"
            with open('/resources/final.ass', 'w', encoding='utf-8') as sf:
                sf.write(content)
        except IOError as ioe:
            logging.error("Couldn't open or write script file: %s", ioe)

    def create_ffmpeg(self, game, episode):
        '''
        Create an container based off the ffmpeg image

        Inputs:
        game        <str> Game to play
        episode:    <int> Episode number to play
        '''
        client = docker.from_env()
        env_dict = {"YOUTUBE_KEY": os.environ["YOUTUBE_KEY"],
                    "DISCORD_WEBHOOK": os.environ["DEBUG_WEBHOOK"],
                    "FFMPEG_VIEWPORT_WIDTH": os.environ["FFMPEG_VIEWPORT_WIDTH"],
                    "FFMPEG_VIEWPORT_HEIGHT": os.environ["FFMPEG_VIEWPORT_HEIGHT"],
                    "FFMPEG_VIEWPORT_X": os.environ["FFMPEG_VIEWPORT_X"],
                    "FFMPEG_VIEWPORT_Y": os.environ["FFMPEG_VIEWPORT_Y"],
                    "FFMPEG_RESOLUTION_WIDTH": os.environ["FFMPEG_RESOLUTION_WIDTH"],
                    "FFMPEG_RESOLUTION_HEIGHT": os.environ["FFMPEG_RESOLUTION_HEIGHT"],
                    "FFMPEG_FORMAT": os.environ["FFMPEG_FORMAT"],
                    "FFMPEG_VCODEC": os.environ["FFMPEG_VCODEC"],
                    "FFMPEG_ACODEC": os.environ["FFMPEG_ACODEC"],
                    "FFMPEG_MINRATE": os.environ["FFMPEG_MINRATE"],
                    "FFMPEG_MAXRATE": os.environ["FFMPEG_MAXRATE"],
                    "FFMPEG_BUFSIZE": os.environ["FFMPEG_BUFSIZE"],
                    "FFMPEG_CRF": os.environ["FFMPEG_CRF"],
                    "FFMPEG_PRESET": os.environ["FFMPEG_PRESET"],
                    "FFMPEG_AUDIO_BITRATE": os.environ["FFMPEG_AUDIO_BITRATE"],
                    "FFMPEG_AR": os.environ["FFMPEG_AR"],
                    "FFMPEG_G": os.environ["FFMPEG_G"],
                    "FFMPEG_PLAYLIST": game,
                    "FFMPEG_EPISODE": episode}
        label_dict = {"image": "ffmpeg",
                      "game": game,
                      "episode": str(episode)}
        container_name = f"ffmpeg_{game.replace(' ', '_')}_E{episode}"
        logging.info("Creating ffmpeg container %s", container_name)
        return client.containers.create(name=container_name,
                                        image="registry.gitlab.com/docker203/ffmpeg:latest",
                                        auto_remove=True,
                                        detach=True,
                                        environment=env_dict,
                                        labels=label_dict,
                                        volumes_from=["zanarkand"])

    def update_status(self):
        # Update status
        write_dict = {
            "episode": self.episode,
            "loop": self.loop,
            "position": self.position
        }

        with open("/resources/status.yaml", 'w', encoding="utf-8") as write_status:
            safe_dump(write_dict,
                      write_status,
                      default_flow_style=False)

def main():
    '''
    main
    '''
    parser = ArgumentParser()
    parser.add_argument("-d", "--debug", help="Enabled debug mode", action="store_true")
    args = parser.parse_args()

    # Set up logging
    log_level = logging.INFO
    if args.debug:
        log_level = logging.DEBUG
    logging.basicConfig(level=log_level,
                        format='%(asctime)s %(levelname)s: %(message)s',
                        datefmt='%d-%b-%y %H:%M:%S')
    logging.info("Starting Stream...")

    # Create the stream object
    stream = Stream()

    while True:

        if stream.container.status != "running":
            logging.info("Starting container %s", stream.container.name)
            stream.container.start()

        # Update the status file
        stream.update_status()

        if stream.episode == 1 and stream.loop == 1:
            msg = f"<:maechen:1138080346475344052> **Stream Maechen**: Now starting playlist **{stream.game.name}** at position **{stream.position}/{stream.length}**."
            if stream.position < stream.length:
                msg += f" Next playlist will be **{stream.schedule[stream.position + 1]}**."
            else:
                msg += f" Next playlist will be **{stream.schedule[0]}**."
            msg += " <:maechen:1138080346475344052>"
            DiscordWebhook(url=os.environ["GENERAL_WEBHOOK"], content=msg).execute()

        # Set up for the next video
        stream.next_video()

        # Sleep so that the video plays the correct subtitles
        sleep(5)

        #Set the subtitles for the next video
        stream.set_subtitles()

        stream.next_container = stream.create_ffmpeg(stream.game.name, stream.episode)

        # Wait until ffmpeg container stops
        stream.container.wait()

        #Update the stream info
        stream.container = stream.next_container

if __name__ == "__main__":
    main()
