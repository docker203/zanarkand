FROM python@sha256:8d6fcdcf73c918683b9933499461b75bd20b5beb08ee96758bed081feb5a2d2d

WORKDIR /zanarkand

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY zanarkand.py /zanarkand/

ENTRYPOINT ["python", "/zanarkand/zanarkand.py"]
